package lab3V4;

public class ViewInfo {
	public String imagePath;
	public String message;
	public double weight;
	public String labelInfo;

	ViewInfo(String imagePath, String message, double weight) {
		this.imagePath = imagePath;
		this.message = message;
		this.weight = weight;
	}

	public ViewInfo getPictureAndMessage() {
		return this;
	}

	ViewInfo() {
	}
}
