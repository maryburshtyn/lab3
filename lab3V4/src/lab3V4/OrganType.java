package lab3V4;

public enum OrganType {
	Brain,
    Mouth,
    Nose,
    Ear,
    Stomach,
    BodyOrgan,
    HeadOrgan
}
