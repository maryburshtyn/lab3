package lab3V4;
/** The class is used to create abstract class Organ.
 */
public abstract class AbstractOrgan {
/**This field is used to define weight of any organ.
 */
    private final double weight;
/**
 * This method is used to define type of organ.
 * @return type of organ
 */
    public abstract OrganType getOrganType();
/**
 * Constructor with parameter for class.
 * @param weight1 weight of organ
 */
    public AbstractOrgan(final double weight1) {
        this.weight = weight1;
    }
/**
 * Default constructor which set weight in 0.
 */
    public AbstractOrgan() {
        this.weight = 0.0;
    }
/**
 * This method is used to get weight of organ.
 * @return weight of organ
 */
    public double getWeight() {
        return this.weight;
    }
}
