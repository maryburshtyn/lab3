package lab3V4;

import head.Head;
import head.Brain;
import head.Ear;
import head.Mouth;
import head.Nose;

/**This class defines Model of data and class diagram.
 */
public class Model {
/**This field contains object of class Head with list of organs.
 */
    private Head head;
/**Default constructor for Model.
 */
    public Model() {
        head = new Head();
    }
/**This method is used to send information to the View {@link lab3V4.View}.
 * @param action operation which will be running
 * @return information of view
 */
    public ViewInfo DispatchAction(final ActionType action) {
        ViewInfo viewInfo = new ViewInfo();
        switch (action) {
          case Eat:
              ((Brain) head.getOrgan(OrganType.Brain)).manager(ActionType.Eat);
              viewInfo.message = ((Brain) head.getOrgan(
                      OrganType.Brain)).getMessage();
              viewInfo.imagePath = "images/eat.gif";
              viewInfo.weight = ((Brain) head.getOrgan(
                      OrganType.Brain)).getWeight();
              viewInfo.labelInfo = "��� �����: " + ((Brain) head.getOrgan(
                      OrganType.Brain)).getEfficiency();
              return viewInfo;
          case Smell:
              ((Brain) head.getOrgan(
                      OrganType.Brain)).manager(ActionType.Smell);
              viewInfo.imagePath = "images/smell.gif";
              viewInfo.message = ((Brain) head.getOrgan(
                      OrganType.Brain)).getMessage();
              viewInfo.weight = ((Brain) head.getOrgan(
                      OrganType.Brain)).getWeight();
              viewInfo.labelInfo = "��� �����:  " + ((Brain) head.getOrgan(
                      OrganType.Brain)).getEfficiency();
              return  viewInfo;
          case Speak:
              viewInfo.message = ((Mouth) head.getOrgan(
                      OrganType.Mouth)).speak();
              viewInfo.imagePath = "images/speak.gif";
              viewInfo.weight = ((Mouth) head.getOrgan(
                      OrganType.Mouth)).getWeight();
              viewInfo.labelInfo = "������ ���: " + ((Mouth) head.getOrgan(
                      OrganType.Mouth)).getSize();
              return  viewInfo;
          case Hear:
              viewInfo.message = ((Ear) head.getOrgan(OrganType.Ear)).hear();
              viewInfo.weight = ((Ear) head.getOrgan(
                      OrganType.Ear)).getWeight();
              ((Brain) head.getOrgan(OrganType.Brain)).setMessage(
                      viewInfo.message);
              viewInfo.message = ((Brain) head.getOrgan(
                      OrganType.Brain)).getMessage();
              viewInfo.imagePath = "images/hear.gif";
              viewInfo.labelInfo = "������� �������: " + ((Ear) head.getOrgan(
                      OrganType.Ear)).getCatkin();
              return  viewInfo;
          case Ich:
              viewInfo.message = ((Nose) head.getOrgan(OrganType.Nose)).itch();
              ((Brain) head.getOrgan(OrganType.Brain)).setMessage(
                      viewInfo.message);
              viewInfo.message = ((Brain) head.getOrgan(
                      OrganType.Brain)).getMessage();
              viewInfo.weight = ((Nose) head.getOrgan(
                      OrganType.Nose)).getWeight();
              viewInfo.imagePath = "images/itch.gif";
              viewInfo.labelInfo = "����� ����: " + ((Nose) head.getOrgan(
                      OrganType.Nose)).getShape();
              return  viewInfo;
          default:
              return  viewInfo;
        }
        //return  viewInfo;
    }
}
