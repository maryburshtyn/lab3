package lab3V4;
/** The class is used to define what operation will be execute.
 * @author Mary Burshtyn
 * @version 1.0
 */
public enum ActionType {
/**Eat is used to define that will be
* execute Mouth method eat().
* See also {@link lab3V4.Mouth#eat(String)}.
*/
    Eat,
/**Smell is used to define that will be
* execute Nose method smell().
* See also {@link lab3V4.Nose#smell(String)}.
*/
    Smell,
/**Ich is used to define that will be
* execute Nose method ich().
* See also {@link lab3V4.Nose#itch()}.
*/
    Ich,
/**Speak is used to define that will be
* execute Mouth method speak().
* See also {@link lab3V4.Mouth#speak()}.
*/
    Speak,
/**Speak is used to define that will be
* execute Ear method hear().
* See also {@link lab3V4.Ear#hear()}.
*/
    Hear
}
