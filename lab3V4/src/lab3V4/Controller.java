package lab3V4;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
/**
 * Class Controller is used to connect
 * class Model {@link lab3V4.Model}
 * and class View {@link lab3V4.View}
 * @author maryb
 */
public class Controller {
/**This field is Model of program (meaning data and hierar�hy).
 */
    private Model theModel;
/**This field is View of program (meaning graphic user interface).
*/
    private View theView;
/**Constructor with parameters.
 * @param model Model
 * @param view View
 */
    public Controller(final Model model, final View view) {
        this.theModel = model;
        this.theView = view;

        this.theView.addEatingButtonListener(
        new CommonListener(ActionType.Eat));
        this.theView.addSmellingButtonListener(
        new CommonListener(ActionType.Smell));
        this.theView.addIchingButtonListener(
        new CommonListener(ActionType.Ich));
        this.theView.addSpeakingButtonListener(
        new CommonListener(ActionType.Speak));
        this.theView.addHearingButtonListener(
        new CommonListener(ActionType.Hear));
    }
/** This class defines common listener for all buttons.
 */
    class CommonListener implements ActionListener {
/**Field which defines operation.
 */
        private ActionType actionType;
/**Constructor with parameters for class CommonListener.
 * @param action defines action which will be running.
 */
        CommonListener(final ActionType action) {
            actionType = action;
        }
/**Overrided method which defines changes in View.
 * @param e some action event
 */
        public void actionPerformed(final ActionEvent e) {
            ViewInfo viewInfo = theModel.DispatchAction(this.actionType);
            theView.thougths.setText("�����: " + viewInfo.message);
            theView.weigth.setText("���: " + viewInfo.weight);
            theView.label.setText(viewInfo.labelInfo);
            theView.ImageLabel.setIcon(new javax.swing.ImageIcon(
                    getClass().getResource(viewInfo.imagePath)));
        }
    }
}
