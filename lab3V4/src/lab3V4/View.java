package lab3V4;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.Image;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class View {
	private JFrame frame;
	private JButton eatingButton, speakingButton, ichingButton, smellingButton,hearingButton;
	JTextField MessageTextField;
	JLabel weigth,thougths,label;
	JLabel ImageLabel;
	Image image;
	JPanel buttonsPanel,lablesPannel;
	
	public View () {
		frame = new JFrame("�������");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(750, 460);
		frame.setBackground(Color.black);
		frame.setResizable(false);
		
		
		BorderLayout borderLayout = new BorderLayout();
		borderLayout.setHgap(10);
		borderLayout.setVgap(10);
		
		lablesPannel = new JPanel();
		lablesPannel.setLayout(new BoxLayout(lablesPannel,BoxLayout.PAGE_AXIS));
		lablesPannel.setBackground(Color.CYAN);
		weigth = new JLabel("���: ");
		lablesPannel.add(weigth);
		label = new JLabel("something");
		lablesPannel.add(label);
		thougths = new JLabel("�����:");
		thougths.setMinimumSize(new Dimension(140,20));
		lablesPannel.add(thougths);
		lablesPannel.add(new JLabel("*******************************************************"));
		
		frame.getContentPane().add(lablesPannel,BorderLayout.LINE_END);
		
		
		
		//frame.getContentPane().add(label,BorderLayout.LINE_END);
		
		buttonsPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
		buttonsPanel.setBackground(Color.YELLOW);
		
		eatingButton = new JButton("����");
		buttonsPanel.add(eatingButton);
		
		speakingButton = new JButton("��������");
		buttonsPanel.add(speakingButton);
		
		ichingButton = new JButton("��������");
		buttonsPanel.add(ichingButton);
		
		smellingButton = new JButton("������");
		buttonsPanel.add(smellingButton);
		
		hearingButton = new JButton("�������");
		buttonsPanel.add(hearingButton);
		
		frame.getContentPane().add(buttonsPanel,BorderLayout.PAGE_END);
		
		ImageLabel = new JLabel();
		ImageLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("images/init.gif")));
		
		frame.getContentPane().add(new JLabel("        "),BorderLayout.LINE_START);
		frame.getContentPane().add(ImageLabel,BorderLayout.CENTER );
		
		
		frame.setVisible(true);
		
	}	
	void addEatingButtonListener (ActionListener listenToEatingButton) {
		eatingButton.addActionListener(listenToEatingButton);
	}
	void addSpeakingButtonListener (ActionListener listenToSpeakingButton) {
		speakingButton.addActionListener(listenToSpeakingButton);
	}
	void addIchingButtonListener (ActionListener listenToIchingButton) {
		ichingButton.addActionListener(listenToIchingButton);
	}
	void addSmellingButtonListener (ActionListener listenToSmellingButton) {
		smellingButton.addActionListener(listenToSmellingButton);
	}
	void addHearingButtonListener (ActionListener listenToSmellingButton) {
		hearingButton.addActionListener(listenToSmellingButton);
	}
	void displayErrorMessage(String errorMessage) {
		JOptionPane.showMessageDialog(frame, errorMessage);
	}

}
