package head;

import lab3V4.OrganType;

/**This class defines field and methods for Ear.
 * Extends from HeadOrgan {@link lab3V4.HeadOrgan}
 */
public class Ear extends HeadOrgan {
/**This field defines presence of catkin.
 */
    private boolean catkin;
/**Message from ears.
 */
    private String hearMessage;
/**Overrided method which return Ear organ type.
 * @return Ear
 */
    @Override
    public  OrganType getOrganType() {
        return OrganType.Ear;
    }
/**Constructor with parameters for Ear class.
 * @param weight weight
 * @param catkn catkin
 */
    public Ear(final double weight, final boolean catkn) {
        super(weight);
        this.catkin = catkn;
    }
/**Method hear return some string.
 * @return hearing information
 */
    public String hear() {
        this.hearMessage = "����� �������� �������";
        return hearMessage;
    }
/**Get information about presence of catkin in ear.
 * @return ���� if it present and ��� if it's not
 */
    public String getCatkin() {
        if (catkin) {
            return "����";
        } else {
            return "���";
        }
    }
}
