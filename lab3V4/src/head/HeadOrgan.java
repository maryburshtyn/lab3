package head;

import lab3V4.AbstractOrgan;
import lab3V4.OrganType;

/**Class HeadOrgan extends from AbstractOrgan.
 */
public abstract class HeadOrgan extends AbstractOrgan {
/**Abstract method which get type of organ.
 */
    public abstract OrganType getOrganType();
/**Constructor with parameters.
 * @param weight weight of organ
 */
    public HeadOrgan(final double weight) {
        super(weight);
    }
}
