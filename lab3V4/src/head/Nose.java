package head;

import lab3V4.OrganType;

public class Nose extends HeadOrgan{
	private String shape;
	private String smellMessage;
	private String itchMessage;
	@Override
	public  OrganType getOrganType() {
		return OrganType.Nose;
	}
	public Nose( double weight, String shape ){
		super(weight);
		this.shape = shape;
	}
	public String itch() {
		this.itchMessage = "��� �������                       ";
		return itchMessage;	
	}
	public String smell() {
		this.smellMessage = "�������� �����                 ";
		return smellMessage;	
	}
	public String getShape() {
		return shape;
	}
}
