package head;

import lab3V4.ActionType;
import lab3V4.OrganType;

/**This class is used to define brain.
 * @author maryb
 */
public class Brain extends HeadOrgan {
/**
 * This field defines efficiency of brain.
 */
    private double efficiency;
/**.
 * This field is link to object of class Mouth
 * {@link lab3V4.Mouth} which is contained in Brain class
 */
    private Mouth mouthLink;
/**.
 * This field is link to object of class Nose
 * {@link lab3V4.Nose} which is contained in Brain class
 */
    private Nose noseLink;
/**.
 * This field is link to object of class Ear
 * {@link lab3V4.Ear} which is contained in Brain class
 */
    private Ear earLink;
/**
 * This field is used to define messages from other organs.
 */
    private String message;
/**
 * This is overrided method which returns that this class is Brain.
 * @return enum type Brain
 */
    @Override
    public  OrganType getOrganType() {
        return OrganType.Brain;
    }
/**Constructor with parameters.
 *  It calls
 * super class constructor {@link lab3V4.HeadOrgans}
 * @param weight1 weight of brain
 * @param efficiency1 efficiency of brain
 * @param mouthLnk Mouth object
 * @param noseLnk Nose object
 * @param earLnk Ear object
 */
    public Brain(final double weight1, final double efficiency1,
        final Mouth mouthLnk, final Nose noseLnk, final Ear earLnk) {
        super(weight1);
        this.efficiency = efficiency1;
        this.mouthLink = mouthLnk;
        this.noseLink = noseLnk;
        this.earLink = earLnk;
    }
/**
 * This method manages organs which is contained in Brain.
 * @param action define action which will be run
 */
    public void manager(final ActionType action) {
        if (action == ActionType.Eat) {
            this.message = mouthLink.eat("Icecream");
        }
        if (action == ActionType.Smell) {
            this.message = noseLink.smell();
        }
        if (action == ActionType.Hear) {
            this.message = earLink.hear();
        }
    }
/**get message from Brain.
 * @return message from Brain
 */
    public String getMessage() {
        return message;
    }
/**set message from other organs which was launched independently.
 * @param msg message from other organs
 */
    public void setMessage(final String msg) {
        this.message = msg;
    }
/**get efficiency of brain.
 * @return efficiency of brain
 */
    public double getEfficiency() {
        return efficiency;
    }
}
