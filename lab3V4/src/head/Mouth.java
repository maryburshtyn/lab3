package head;


import java.util.Random;

import body.Stomach;
import lab3V4.OrganType;

public class Mouth extends HeadOrgan {
	private double size;
	private Stomach stomachLink;
	private String eatMessage;
	@Override
	public  OrganType getOrganType() {
		return OrganType.Mouth;
	}
	public Mouth(double weight, double size,Stomach stomachLink ){
		super(weight);
		this.stomachLink = stomachLink;
		this.size = size;
	}
	public String speak() {
		String[] array = new String[5];
		array[0] = "�������� �������";
		array[1] = "������ ����� ����� �������?";
		array[2] = "����� ������ ����";
		array[3] = "�������� ������� �� ���������";
		array[4] = "� �����.";
		final Random random = new Random();
		String message = array[random.nextInt(5)];
		return message;	
	}
	public String eat(String food) {
		String[] array = new String[5];
		array[0] = "Delicious!";
		array[1] = "���.. ����� �������!";
		array[2] = "� ���� ��� ���?";
		array[3] = "����� �������, ����� ��������..";
		array[4] = "�� ���, � �������!";
		final Random random = new Random();
		this.eatMessage = array[random.nextInt(5)];;
		this.sendFood(food);
		return eatMessage;	
	}
	public void sendFood(String food) {
		this.stomachLink.getFood(food);
		this.stomachLink.digest();
	}
	public double getSize() {
		return size;
	}
}
