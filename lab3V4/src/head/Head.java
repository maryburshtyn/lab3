package head;

//TO DO:
//������� ����� ��������� �����
//Checkstyle
//javadoc
//�� ����������� JUnit
import java.util.*;

import body.Body;
import lab3V4.OrganType;
import body.Stomach;
/**Class Head which contain ArrayList of head organ type organs.
* Like Nose {@link lab3V4.Nose}
* Mouth {@link lab3V4.Mouth}
* Ear {@link lab3V4.Ear}
* @author maryb
*
*/
public class Head {
/**BRAIN_EFFICIENCY constant for specifying the weight of brain.
*/
  private static final double BRAIN_EFFICIENCY = 0.3;
/**BRAIN_WEIGHT constant for specifying the weight of brain.
*/
  private static final double BRAIN_WEIGHT = 1.7;
/**EAR_WEIGHT constant for specifying the weight of ear.
*/
  private static final boolean PRESENCE_OF_CATKIN = true;
/**EAR_WEIGHT constant for specifying the weight of ear.
*/
  private static final double EAR_WEIGHT = 0.8;
/**MOUTH_SIZE constant for specifying the size of mouth.
*/
  private static final double MOUTH_SIZE = 10.1;
/**MOUTH_WEIGHT constant for specifying the weight of mouth.
*/
  private static final double MOUTH_WEIGHT = 0.7;
/**NOSE_WEIGHT constant for specifying the weight of nose.
*/
  private static final double NOSE_WEIGHT = 0.5;
/**NOSE_SHAPE constant for specifying the shape of nose.
*/
  private static final String NOSE_SHAPE = "������";
/**This field is array list of organs.
*/
  private ArrayList<HeadOrgan> headOrganList;
/**Method which add organ to headOrganList.
* @param headOrgan organ to be added to list
*/
  public void addOrgan(final HeadOrgan headOrgan) {
      headOrganList.add(headOrgan);
  }
/**Method which returns organ from list by their name.
* @param organType Type of organ
* @return object of HeadOrgan
*/
  public HeadOrgan getOrgan(final OrganType organType) {
      for (int i = 0; i < this.headOrganList.size(); i++) {
          HeadOrgan o = this.headOrganList.get(i);
          if (o.getOrganType() == organType) {
              return (HeadOrgan) o;
          }
      }
      return null;
  }
/**Default constructor.
*/
  public Head() {
      headOrganList = new ArrayList<HeadOrgan>();
      Nose nose = new Nose(NOSE_WEIGHT, NOSE_SHAPE);
      Body body = new Body();
      Mouth mouth = new Mouth(MOUTH_WEIGHT, MOUTH_SIZE,
            ((Stomach) body.getOrgan(OrganType.Stomach)));
      Ear ear = new Ear(EAR_WEIGHT, PRESENCE_OF_CATKIN);
      Brain brain = new Brain(BRAIN_WEIGHT,
              BRAIN_EFFICIENCY, mouth, nose, ear);
      this.addOrgan(nose);
      this.addOrgan(mouth);
      this.addOrgan(ear);
      this.addOrgan(brain);
  }
}
