package body;

import lab3V4.OrganType;

public class Stomach extends BodyOrgan {
	private double volume;
	private String food = "";
	@Override
	public  OrganType getOrganType() {
		return OrganType.Stomach;
	}
	public Stomach(double weight, double volume ){
		super(weight);
		this.volume = volume;
	}
	public void getFood(String food) {
		this.food = food;
	}
	public String digest(){
		if(this.food!="")
			return "����������";
		else return " E�� ���:(";
	}
}
