package body;

import lab3V4.AbstractOrgan;
import lab3V4.OrganType;

/** The class is used to create class of body type organ.
 * It extends from AbstractOrgan class {@link lab3V4.AbstractOrgan}
 */
public class BodyOrgan extends AbstractOrgan {
/**
 * This method returns BodyOrgan type from enum type.
 * @return BodyOrgan type
 */
    public OrganType getOrganType() {
        return OrganType.BodyOrgan;
    }
/**
 * Constructor with parameters for class BodyOrgan.
 * @param weight weight of organ
 */
    public BodyOrgan(final double weight) {
        super(weight);
        }
/**Default constructor.
 * It calls super
 * default constructor {@link lab3V4.AbstractOrgan}
 */
    public BodyOrgan() {
        super();
        }
}
