package body;

import java.util.ArrayList;
/**
 * This class consist list on body type organs.
 * @author maryb
 *
 */
public class Body {
/**STOMACH_WEIGHT constant for specifying the weight of stomach.
*/
    private static final double STOMACH_WEIGHT = 0.5;
/**STOMACH_VOLUME constant for specifying the volume of stomach.
*/
    private static final double STOMACH_VOLUME = 4.0;
/**
 * This field is a list of body type organs for example stomach.
 */
    private ArrayList<BodyOrgan> bodyOrganList;
/**
 * This method add new organ to list of body organs.
 * @param bodyOrgan new organ
 */
    public void addOrgan(final BodyOrgan bodyOrgan) {
        bodyOrganList.add(bodyOrgan);
    }
/**
 * Default constructor. Define new object of class Stomach
 * {@link lab3V4.Stomach} and add them to list of body organs}
 */
    public Body() {
        Stomach stomach = new Stomach(STOMACH_WEIGHT, STOMACH_VOLUME);
        bodyOrganList = new ArrayList<BodyOrgan>();
        this.addOrgan(stomach);
    }
/**
 * This method return organ from list of organs by the name of organ.
 * @param organType name of organ
 * @return object of class BodyOrgan {@link lab3V4.BodyOrgan}}
 */
    public BodyOrgan getOrgan(final OrganType organType) {
        for (int i = 0; i < this.bodyOrganList.size(); i++) {
            BodyOrgan o = this.bodyOrganList.get(i);
            if (o.getOrganType() == organType) {
                return (BodyOrgan) o;
            }
        }
        return null;
    }
}
